$(document).ready(function(){

  $('.owl-carousel').owlCarousel({
    loop:false,
    margin:0,
    nav:true,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    responsive:{
      0:{
          items:1
      },
      991:{
          items:2
      },
      1000:{
          items:2
      }
    }
  });

});

// Adicionar função mouse
// document.addEventListener("scroll", () => {
//   const rolagemcount = document.documentElement.scrollHeight - window.innerHeight;
//   const rolagem = window.scrollY;

//   if(rolagem === rolagemcount) {
//     alert("Você ativou!");
//   }
// });

// Função aparecer com o scroll
// $(document).ready(function () {
//   $(window).scroll(function () {
//     let windowScroll = $(window).scrollTop();

//     if (windowScroll > 465) {
//       $(".nav-interno").fadeIn();
//     } else {
//       $(".nav-interno").fadeOut();
//     }
//   });
// // Função clicar o voltar ao topo
//   $("html,body").click(function () {
//     $(".nav-interno").animate({
//       scrollTop: $("html").offset().top,
//     }, 700);
//     return false;
//   });
// });

// Adicionar sombra no nav interno e exibir o botão para voltar ao topo da página
$(window).scroll(function() {     
  var scroll = $(window).scrollTop();
  if ($(".nav-interno").offset().top <= scroll) {
    $(".nav-interno").addClass("shadow");
    $(".button-to-top").addClass('show');
  } else {
    $(".nav-interno").removeClass("shadow");
    $(".button-to-top").removeClass('show');
  }
});

// Botão para voltar ao topo da página
$(".button-to-top").on('click', function() {
  $('html, body').animate({scrollTop:0}, '500');
});

const html = document.querySelector("html")
const checkbox = document.querySelector("input[name=theme]")

const getStyle = (element, style) => 
    window
        .getComputedStyle(element)
        .getPropertyValue(style)


const initialColors = {
    bg: getStyle(html, "--bg"),
    bgPanel: getStyle(html, "--bg-panel"),
    colorHeadings: getStyle(html, "--color-headings"),
    colorText: getStyle(html, "--color-text"),
}

const darkMode = {
    bg: "#000000",
    bgPanel: "#434343",
    colorHeadings: "#3664FF",
    colorText: "#B5B5B5"
}

const transformKey = key => 
    "--" + key.replace(/([A-Z])/, "-$1").toLowerCase()


const changeColors = (colors) => {
    Object.keys(colors).map(key => 
        html.style.setProperty(transformKey(key), colors[key]) 
    )
}


checkbox.addEventListener("change", ({target}) => {
    target.checked ? changeColors(darkMode) : changeColors(initialColors)
})


let toggleInverted = document.getElementById('inverted');

if (toggleInverted.checked) {
  document.querySelector("#inverted").checked = true;
}else {
  document.querySelector("#inverted").checked = false;
}


function toggleZoom() {
  document.body.style.zoom = "150%";
}


